# dotfiles <!-- omit in toc -->

This directory contains the dotfiles for my system

# Table of contents <!-- omit in toc -->
- [Compatibility](#compatibility)
- [Requirements](#requirements)
- [Installation](#installation)
- [Scripts](#scripts)
- [Tips](#tips)

## Compatibility

This repository and all configurations and scripts are compatible with the following operating systems:

- Fedora
- Ubuntu
- Pop!_OS

## Requirements

Ensure you have the following installed on your system

**RHEL/Fedora:**
```bash
sudo dnf install -y stow git
```

**Debian/Ubuntu:**
```bash
sudo apt install -y stow git
```

## Installation

First, clone the dotfiles repository into your `$HOME` directory using one of the methods below:

**Clone via SSH:**
```bash
git clone --recursive git@gitlab.com:courtlane/dotfiles.git ~/dotfiles
```
**Clone via HTTPS:**
```bash
git clone --recursive https://gitlab.com/courtlane/dotfiles.git ~/dotfiles
```

Use GNU `stow` to create symlinks for all dotfiles in `$HOME`.<br>
You may need to rename your existing dotfiles because they may conflict with the `stow` command.

```bash
# Create backup of bashrc
cp ~/.bashrc ~/.bashrc.bak && rm ~/.bashrc

# Create symlinks from dotfiles using stow
cd ~/dotfiles
stow . -v
```

Run tmux/vim and download all plugins

```md
# Vim
:PlugInstall

# Tmux
<Prefix> + I
```

## Scripts

You may use the scripts in the [`scripts`](./scripts) directory to install all the necessary tools that are required for the dotfiles in this repository.

Run `init.sh` to execute all scripts (without optional scripts)

```bash
~/dotfiles/scripts/init.sh
```

**Optional scripts:**

The following scripts will not run automatically

```bash
# Install VSCodium and the extensions in codium_extensions.txt
~/dotfiles/scripts/install_vscodium.sh --extensions ~/dotfiles/scripts/codium_extensions.txt

# Install Flatpak and the packages in flatpak-packages.txt
~/dotfiles/scripts/install_flatpak.sh --packages ~/dotfiles/scripts/flatpak-packages.txt
```

**Deprecated scripts:**

The following are still available but not used anymore:

- [`install_lazygit`](./scripts/install_lazygit.sh)
- [`install_oh-my-zsh`](./scripts/install_oh-my-zsh.sh)
- [`install_oh-my-posh`](./scripts/install_oh-my-posh.sh)

## Tips

**Restow all symlinks (This will clean all existing symlinks and stow them again):**

```bash
cd ~/dotfiles
stow -R . -v
```

**Delete existing symlinks:**

```bash
cd ~/dotfiles
stow -D . -v
```

**Update .Brewfile:**

```bash
# Check Homebrew env
echo $HOMEBREW_BUNDLE_FILE_GLOBAL

# Export installed Homebrew packages to .Brewfile
brew bundle dump --global --force

# Install packages from .Brewfile
brew bundle install --global
```

