# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
    for rc in ~/.bashrc.d/*; do
        if [ -f "$rc" ]; then
            . "$rc"
        fi
    done
fi
unset rc

#---------------------------------------------------------
# History config
#---------------------------------------------------------

# Avoid duplicates..
export HISTCONTROL=ignoredups:erasedups

# Append history entries..
shopt -s histappend

# History size
export HISTSIZE=100000

# Prevent history appearance for commands starting with space
export HISTIGNORE=' *'

# Shared history for tmux or multiple terminal windows - append and reload history immediately
if [[ "$PROMPT_COMMAND" != *"history -a"* && "$PROMPT_COMMAND" != *"history -c"* && "$PROMPT_COMMAND" != *"history -r"* ]]; then
    # Append the history commands to PROMPT_COMMAND if not present
    export PROMPT_COMMAND="$PROMPT_COMMAND;history -a;history -c;history -r"
fi

#---------------------------------------------------------
# Aliases
#---------------------------------------------------------

alias vi='vim'
alias ls='ls --color'
alias ll='eza -l --icons --git'
alias la='eza -l --icons --git -a'
alias lt='eza --tree --level=2 --long --icons --git'
alias cat='bat --style=plain --theme=OneHalfDark --paging=never'
alias lg='lazygit'
alias dps='docker ps --format "table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Status}}\t{{.CreatedAt}}\t{{.Size}}"'

# tmux
alias tmuxl='tmux ls'
alias tmuxa='tmux attach-session -t'
alias tmuxk='tmux kill-session -t'
alias tmuxn='tmux new-session -s'

# oh-my-posh
alias posh='eval "$(oh-my-posh init bash --config ~/.config/oh-my-posh/themes/amro.omp.json)"'
alias poshk8s='eval "$(oh-my-posh init bash --config ~/.config/oh-my-posh/themes/amro.omp_kubectl.json)"'

# xsel clipboard
alias pbcopy='xsel --input --clipboard'
alias pbpaste='xsel --output --clipboard'

# vscodium wayland nvidia issues
alias codium='codium --ozone-platform=wayland'

# git
alias gl='git pull'
alias gpr='git pull --rebase'
alias gp='git push'
alias gpd='git push --dry-run'
alias gst='git status'
alias gss='git status --short'
alias gsi='git submodule init'
alias gsu='git submodule update'

# kubectl
alias k=kubectl
alias kdel='kubectl delete'
# Contexts
alias kcgc='kubectl config get-contexts'
alias kcuc='kubectl config use-context'
alias kcsc='kubectl config set-context'
alias kcdc='kubectl config delete-context'
alias kccc='kubectl config current-context'
# Pod management.
alias kgp='kubectl get pods'
alias kdp='kubectl describe pods'
# k9s
alias ks=k9s

# Completion for k alias
complete -o default -F __start_kubectl k

#---------------------------------------------------------
# Custom variables
#---------------------------------------------------------

# Preferred editor
export EDITOR='vim'

## Kubernetes variables
export do="--dry-run=client -o yaml"    # k create deploy nginx --image=nginx $do
export now="--force --grace-period 0"   # k delete pod x $now

# Python
export REQUESTS_CA_BUNDLE='/etc/ssl/certs/ca-certificates.crt'
export SSL_CERT_FILE='/etc/ssl/certs/ca-certificates.crt'

#---------------------------------------------------------
# Custom functions
#---------------------------------------------------------

# Kubernetes get all resources
kall() {
  (echo -e "NAME\tNAMESPACE\tKIND\tTIMESTAMP" && kubectl get $(kubectl api-resources --verbs=list -o name | tr '\n' ',' | sed 's/,$//') --all-namespaces -o json | jq -r '
    .items[] |
    [.metadata.name, (.metadata.namespace // ""), (.kind | ascii_downcase), (.metadata.creationTimestamp // "N/A")] |
    @tsv
  ') | column -t -s $'\t'
}

#---------------------------------------------------------
# Additional packages and functions
#---------------------------------------------------------

# Homebrew package manager
if [ -x "/home/linuxbrew/.linuxbrew/bin/brew" ]; then
  eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

  # Homebrew bash-completion
  if type brew &>/dev/null
  then
    HOMEBREW_PREFIX="$(brew --prefix)"
  	HOMEBREW_BUNDLE_FILE_GLOBAL="~/.Brewfile"
    if [[ -r "${HOMEBREW_PREFIX}/etc/profile.d/bash_completion.sh" ]]
    then
      source "${HOMEBREW_PREFIX}/etc/profile.d/bash_completion.sh"
    else
      for COMPLETION in "${HOMEBREW_PREFIX}/etc/bash_completion.d/"*
      do
        [[ -r "${COMPLETION}" ]] && source "${COMPLETION}"
      done
    fi
  fi
fi

# fzf config
eval "$(fzf --bash)"
# optional bind ctrl+space to ctrl+r
#bind '"\C- ": "\C-r"'

# Zoxide cd replacement
eval "$(zoxide init --cmd cd bash)"

#---------------------------------------------------------
# Bash Autocompletion
#---------------------------------------------------------

# Terragrunt
if type terragrunt &>/dev/null; then [ -f "$(which terragrunt)" ] && complete -C "$(which terragrunt)" terragrunt; fi

# Terraform
if type terraform &>/dev/null; then [ -f "$(which terraform)" ] && complete -C "$(which terraform)" terraform; fi

# Vault
if type vault &>/dev/null; then [ -f "$(which vault)" ] && complete -C "$(which vault)" vault; fi

#---------------------------------------------------------
# Oh-My-Posh prompt config
#---------------------------------------------------------

eval "$(oh-my-posh init bash --config ~/.config/oh-my-posh/themes/amro.omp.json)"
