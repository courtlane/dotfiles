# -- General --------------

export PATH=$HOME/bin:$HOME/.local/bin:/usr/local/bin:$PATH

# Set LS_COLORS via GNU dircolors
if type dircolors &>/dev/null; then
	source <(dircolors -b)
fi

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='vim'
fi

# -- Homebrew --------------

# Load Homebrew package manager
if [ -x "/home/linuxbrew/.linuxbrew/bin/brew" ]; then
  eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

  if type brew &>/dev/null
  then
		# Add autocompletion for Homebrew packages
    FPATH="$HOMEBREW_PREFIX/share/zsh/site-functions:${FPATH}"

		# Global Brewfile
  	HOMEBREW_BUNDLE_FILE_GLOBAL="~/.Brewfile"
  fi
fi

# -- Prompt --------------

# Load Oh-My-Posh
eval "$(oh-my-posh init zsh --config ~/.config/oh-my-posh/themes/amro.omp.json)"

# -- Zinit plugin manager --------------

# Set the directory we want to store zinit and plugins
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"

# Download Zinit, if it's not there yet
if [ ! -d "$ZINIT_HOME" ]; then
   mkdir -p "$(dirname $ZINIT_HOME)"
   git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
fi

# Load Zinit
source "${ZINIT_HOME}/zinit.zsh"

# ZSH plugins
zinit light zsh-users/zsh-autosuggestions

# Snippets (OMZP Oh-My-Zsh-Plugins)
zinit snippet OMZP::git
zinit snippet OMZP::sudo
zinit snippet OMZP::kubectl
zinit snippet OMZP::command-not-found

# -- Completion --------------

# Create zcompdump file for loading completions much faster from $FPATH
if [[ -z "$ZSH_COMPDUMP" ]]; then
  ZSH_COMPDUMP="${ZDOTDIR:-$HOME}/.zcompdump-${HOST}-${ZSH_VERSION}"
fi

# Load completions
autoload -Uz compinit                            # Load and initialize the completion system
compinit -i -d "$ZSH_COMPDUMP"                   # Initialize completions with the specified dump file
autoload -U +X bashcompinit && bashcompinit      # Enable bash completion compatibility

# Replay compdefs (to be done after compinit)
zinit cdreplay -q

# Case-sensitive completion
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|=*' 'l:|=* r:|=*'

# Use LS_COLORS for completion list colors
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"

# Enable autocompletion for special directories like .. to ../
zstyle ':completion:*' special-dirs true

# -- Keybindings --------------

bindkey -e                            # Use emacs keybindings
bindkey '^p' history-search-backward  # Search history backward using Ctrl+P
bindkey '^n' history-search-forward   # Search history forward using Ctrl+N
bindkey '^ ' autosuggest-accept       # Accept autosuggestion with Ctrl+Space

# Enable '/' as a word separator for quicker path navigation, similar to the default behavior in Bash, facilitating CTRL+Arrow key shortcuts."
autoload -Uz select-word-style
select-word-style bash

# CTRL+W efficiently deletes both the word and its leading '-' character
zstyle ':zle:backward-kill-word' word-style whitespace-subword

# -- History --------------

HISTSIZE=100000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase            			 # Erase old duplicate entries when a new one is added to history
setopt appendhistory           # Append new history lines to the history file
setopt sharehistory            # Share history across all sessions
setopt hist_ignore_space       # Commands starting with a space will be ignored
setopt hist_ignore_all_dups    # Remove all previous entries matching the new command
setopt hist_save_no_dups       # Do not save duplicate entries in the history file
setopt hist_ignore_dups        # Ignore duplicate commands in the current session
setopt hist_find_no_dups       # Ignore duplicates when searching through history

# -- Options --------------

setopt shwordsplit             # Allows interpretation of variables in commands, e.g., kubernetes $do
setopt interactive_comments    # Treat lines starting with # as comments when pasted
setopt dot_glob                # Include hidden files when using * in globbing

# -- Aliases --------------

alias vi='vim'
alias ls='ls --color'
alias ll='eza -l --icons --git'
alias la='eza -l --icons --git -a'
alias lt='eza --tree --level=2 --long --icons --git'
alias cat='bat --style=plain --theme=OneHalfDark --paging=never'
alias lg='lazygit'
alias hist='history 0'
alias dps='docker ps --format "table {{.ID}}\t{{.Names}}\t{{.Image}}\t{{.Status}}\t{{.CreatedAt}}\t{{.Size}}"'

# tmux
alias tmuxl='tmux ls'
alias tmuxa='tmux attach-session -t'
alias tmuxk='tmux kill-session -t'
alias tmuxn='tmux new-session -s'

# oh-my-posh
alias posh='eval "$(oh-my-posh init bash --config ~/.config/oh-my-posh/themes/amro.omp.json)"'
alias poshk8s='eval "$(oh-my-posh init bash --config ~/.config/oh-my-posh/themes/amro.omp_kubectl.json)"'

# xsel clipboard
alias pbcopy='xsel --input --clipboard'
alias pbpaste='xsel --output --clipboard'

# vscodium wayland nvidia issues (Should be fixed with new nvidia drivers 555.x)
#alias codium='codium --ozone-platform=wayland'

# k9s
alias ks=k9s

# -- Custom variables --------------

# FZF Theme
export FZF_DEFAULT_OPTS=" \
--color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
--color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
--color=marker:#b4befe,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8 \
--color=selected-bg:#45475a \
--multi"

# Kubernetes
export do="--dry-run=client -o yaml"    # k create deploy nginx --image=nginx $do
export now="--force --grace-period 0"   # k delete pod nginx $now

# Kubernetes Krew plugin manager
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

# Python
export REQUESTS_CA_BUNDLE='/etc/ssl/certs/ca-certificates.crt'
export SSL_CERT_FILE='/etc/ssl/certs/ca-certificates.crt'

# -- Custom functions --------------

# Kubernetes get all resources
kall() {
  (echo -e "NAME\tNAMESPACE\tKIND\tTIMESTAMP" && kubectl get $(kubectl api-resources --verbs=list -o name | tr '\n' ',' | sed 's/,$//') --all-namespaces -o json | jq -r '
    .items[] |
    [.metadata.name, (.metadata.namespace // ""), (.kind | ascii_downcase), (.metadata.creationTimestamp // "N/A")] |
    @tsv
  ') | column -t -s $'\t'
}

# -- Shell integrations --------------

eval "$(fzf --zsh)"
eval "$(zoxide init --cmd cd zsh)"
