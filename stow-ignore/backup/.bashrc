# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc

complete -C /usr/bin/terraform terraform

export EDITOR=/usr/bin/vim

# avoid duplicates..
export HISTCONTROL=ignoredups:erasedups

# append history entries..
shopt -s histappend

# history size
export HISTSIZE=10000

# prevent history appearance for commands starting with space
export HISTIGNORE=' *'

# shared history - append and reload history immediately
export PROMPT_COMMAND="history -a; history -r; $PROMPT_COMMAND"

# Oh my posh config
#eval "$(oh-my-posh init bash --config ~/.poshthemes/montys_custom.omp.json)"
#eval "$(oh-my-posh init bash --config ~/.poshthemes/amro.omp.json)"

complete -C /usr/bin/vault vault

# bat command paging
export BAT_PAGER=never

# Aliases
alias ll="exa -l"
alias k=kubectl

# kubectl bash completion
complete -o default -F __start_kubectl k
source <(kubectl completion bash)

# Minio Client completion and compatibility
complete -C /home/franko/bin/mc mc

# PATH 
export PATH=$PATH:$HOME/minio-binaries/

export REQUESTS_CA_BUNDLE='/etc/ssl/certs/ca-certificates.crt'
