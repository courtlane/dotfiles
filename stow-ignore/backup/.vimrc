set expandtab
set tabstop=2
set incsearch
set hlsearch
set paste
		
call plug#begin()

Plug 'preservim/nerdtree'
Plug 'solvedbiscuit71/vim-autopair'
Plug 'airblade/vim-gitgutter'
Plug 'yggdroot/indentline'
Plug 'adrienverge/yamllint'
Plug 'wellle/tmux-complete.vim'
Plug 'roxma/vim-tmux-clipboard'
Plug 'christoomey/vim-system-copy'
Plug 'rrethy/vim-illuminate'

" Colorschemes
Plug 'cocopon/iceberg.vim'

call plug#end()

" Mapping
nnoremap <C-n> :NERDTreeToggle<CR>
nnoremap <C-m> :NERDTreeFocus<CR>
nmap <F1> <nop>

" YAML
let g:indentLine_char = '⦙'

" Statusline
"set laststatus=2
autocmd VimEnter * :file

" Colorscheme
"let g:gruvbox_contrast_dark = 'hard'
autocmd VimEnter * hi link illuminatedWord Visual
set background=dark
colorscheme iceberg
