" General
set expandtab
set tabstop=2
set incsearch
set hlsearch
set paste
set noshowcmd

" Toggle highlights search with Leader (default is \) + Space
nnoremap <silent> <Leader><Space> :nohlsearch<CR>

" Disable F1 help
nmap <F1> <nop>

" Statusline
"set laststatus=2 " automatically enabled in split navigation
set statusline+=%F\ %l:%c\ %p%%

" Ruler (top right corner status)
set ruler
set rulerformat=%60(%=%F\ %l:%c\ %p%%%)

" Remap Ctrl + W to CTRL + for split navigation
nnoremap <silent> <C-K> <C-W><Up>
nnoremap <silent> <C-J> <C-W><Down>
nnoremap <silent> <C-H> <C-W><Left>
nnoremap <silent> <C-L> <C-W><Right>

" Disable autocmd that initially shows the path on the bottom
autocmd VimEnter * echo "" | redraw!

" Plugins
call plug#begin()

Plug 'preservim/nerdtree'
Plug 'rrethy/vim-illuminate'
Plug 'airblade/vim-gitgutter'
Plug 'yggdroot/indentline'
Plug 'roxma/vim-tmux-clipboard'

" Colorschemes
Plug 'cocopon/iceberg.vim'

call plug#end()

" NerdTree Mapping
nnoremap <C-n> :NERDTreeToggle<CR>
nnoremap <C-m> :NERDTreeFocus<CR>

" Vim-Gitgutter Mapping
nnoremap <C-g> :GitGutterToggle<CR>

" Vim-Illuminate (highlighting other uses of the current word under the cursor)
autocmd VimEnter * hi link illuminatedWord Visual

" Colorscheme
set background=dark
colorscheme iceberg
