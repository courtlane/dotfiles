#!/bin/bash

# Get the directory containing the script
script_dir="$(cd "$(dirname "$0")" && pwd)"

# Include scripts
bash "$script_dir/install_packages.sh"
bash "$script_dir/install_nerdfont.sh"
bash "$script_dir/install_vim-plug.sh"
bash "$script_dir/install_homebrew.sh"

