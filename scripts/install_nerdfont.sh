#!/bin/bash

# Function to display INFO messages
info() {
    echo -e "\e[96m[INFO]\e[0m $1"
}

# Function to display ERROR messages and exit script
error() {
    echo -e "\e[91m[ERROR]\e[0m $1"
    exit 1
}

# Download Hack Nerd Font
info "Downloading Hack Nerd Font..."
curl -L https://github.com/ryanoasis/nerd-fonts/releases/download/v3.1.1/Hack.zip -o /tmp/Hack.zip || error "Failed to download Hack Nerd Font"

# Create fonts directory
info "Creating fonts directory..."
mkdir -p ~/.local/share/fonts || error "Failed to create fonts directory"

# Unzip fonts into ~/.local/share/fonts
info "Unzipping fonts into ~/.local/share/fonts..."
unzip -q /tmp/Hack.zip -d ~/.local/share/fonts/ -x "*.md" || error "Failed to unzip fonts"

# Build font cache
info "Building font cache..."
fc-cache -f -v || error "Failed to build font cache"

info "Hack Nerd Font installation completed successfully."

