#!/bin/bash

# Function to display INFO messages
info() {
    echo -e "\e[96m[INFO]\e[0m $1"
}

# Function to install packages on Fedora-based systems
install_on_rhel() {
    info "Installing packages with dnf"
    sudo dnf install alacritty git curl wget unzip bash-completion bind-utils zsh vim neovim tmux eza ripgrep fd-find docker docker-compose gnome-themes-extra gnome-tweaks python3-pip tree tldr sslscan xsel
}

# Function to install packages on Debian-based systems
install_on_debian() {
    info "Installing packages with apt"
    sudo apt update
	sudo apt install -y alacritty git curl wget unzip bash-completion dnsutils zsh vim neovim tmux ripgrep fd-find docker.io docker-compose gnome-themes-extra gnome-tweaks python3-pip tree tldr sslscan xsel
}

# Determine the Linux distribution
if [ -r /etc/os-release ]; then
    . /etc/os-release
    if [ "$ID" = "rhel" ] || [ "$ID" = "centos" ] || [ "$ID" = "fedora" ]; then
        install_on_rhel
    elif [ "$ID" = "debian" ] || [ "$ID" = "ubuntu" ] || [ "$ID" = "pop" ]; then
        install_on_debian
    else
        info "Unsupported distribution: $ID"
        exit 1
    fi
else
    info "Cannot determine Linux distribution."
    exit 1
fi

