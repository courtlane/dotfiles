#!/bin/bash

# Function to display INFO messages
info() {
    echo -e "\e[96m[INFO]\e[0m $1"
}

# Get the latest LazyGit version
info "Fetching the latest LazyGit version..."
LAZYGIT_VERSION=$(curl -s "https://api.github.com/repos/jesseduffield/lazygit/releases/latest" | grep -Po '"tag_name": "v\K[^"]*')

# Download the tar archive to /tmp
info "Downloading LazyGit..."
curl -Lo /tmp/lazygit.tar.gz "https://github.com/jesseduffield/lazygit/releases/latest/download/lazygit_${LAZYGIT_VERSION}_Linux_x86_64.tar.gz"

# Extract the tar archive into /tmp
info "Extracting LazyGit into /tmp..."
tar -xf /tmp/lazygit.tar.gz -C /tmp lazygit

# Install LazyGit to /usr/local/bin
info "Installing LazyGit..."
sudo install /tmp/lazygit /usr/local/bin

# Clean up: remove the downloaded tar archive and extracted files
info "Cleaning up..."
rm /tmp/lazygit.tar.gz /tmp/lazygit

info "LazyGit $LAZYGIT_VERSION installed successfully."

