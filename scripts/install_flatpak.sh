#!/bin/bash

# Function to display INFO messages
info() {
    echo -e "\e[96m[INFO]\e[0m $1"
}

# Function to display usage information
usage() {
    echo "Usage: $0 [OPTIONS]"
    echo "Options:"
    echo "  --packages <path>   Specify the path to file with a list of flatpak packages."
    echo "  --help              Display this help message."
}

# Display usage information when --help or -h option is provided
if [[ "$1" == "--help" || "$1" == "-h" ]]; then
    usage
    exit 0
fi

# Function to install dependencies on RHEL-based systems
install_on_rhel() {
    info "Installing flatpak on RHEL-based systems"
		sudo dnf install flatpak -y
}

# Function to install packages on Debian-based systems
install_on_debian() {
    info "Installing flatpak on Debian-based systems"
    sudo apt update
		sudo apt install flatpak gnome-software-plugin-flatpak -y
}

install_flatpak_packages() {
    local packages_file="$1"

    # Check if flatpak-packages.txt exists
    if [ -f "$packages_file" ]; then
        info "Installing flatpaks from $packages_file"
        # Read each package name from flatpak-packages.txt and install it
        while IFS= read -r package; do
            flatpak install --noninteractive flathub "$package" -y
        done < "$packages_file"
    else
        echo "Error: $packages_file not found." >&2
        exit 1
    fi
}

# Determine the Linux distribution
if [ -r /etc/os-release ]; then
    . /etc/os-release
    if [ "$ID" = "rhel" ] || [ "$ID" = "centos" ] || [ "$ID" = "fedora" ]; then
        install_on_rhel
    elif [ "$ID" = "debian" ] || [ "$ID" = "ubuntu" ] || [ "$ID" = "pop" ]; then
        install_on_debian
    else
        info "Unsupported distribution: $ID"
        exit 1
    fi

		# Add flathub repo
		info "Add flathub repository"
		flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo

    # Check for options
    while [[ $# -gt 0 ]]; do
        case "$1" in
            --packages)
                shift
                install_flatpak_packages "$1"
                ;;
            --help)
                usage
                exit 0
                ;;
            *)
                echo "Error: Invalid option: $1" >&2
                usage
                exit 1
                ;;
        esac
        shift
    done

else
    info "Cannot determine Linux distribution."
    exit 1
fi
