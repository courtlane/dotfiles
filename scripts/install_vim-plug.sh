#!/bin/bash

# Function to display INFO messages
info() {
	echo -e "\e[96m[INFO]\e[0m $1"
}

# Install vim-plug
info "Install vim-plug"
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
