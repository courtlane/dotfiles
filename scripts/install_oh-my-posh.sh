#!/bin/bash

# Function to display INFO messages
info() {
    echo -e "\e[96m[INFO]\e[0m $1"
}

# Define the path to the ~/bin directory
bin_dir="$HOME/bin"

# Check if the ~/bin directory exists
if [ ! -d "$bin_dir" ]; then
    info "$bin_dir directory does not exist. Creating..."
    mkdir -p "$bin_dir"
else
    info "Directory $bin_dir already exists."
fi

# Install oh-my-posh
info "Install oh-my-posh"
curl -s https://ohmyposh.dev/install.sh | bash -s -- -d "$HOME/bin"
