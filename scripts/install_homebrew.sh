#!/bin/bash

# Function to display INFO messages
info() {
    echo -e "\e[96m[INFO]\e[0m $1"
}

# Function to install  dependencies on RHEL-based systems
install_on_rhel() {
    info "Installing homebrew dependencies with dnf"
		sudo dnf groupinstall 'Development Tools'
		sudo dnf install procps-ng curl file git
}

# Function to install packages on Debian-based systems
install_on_debian() {
    info "Installing homebrew dependencies with apt"
    sudo apt update
		sudo apt-get install build-essential procps curl file git
}

# Determine the Linux distribution
if [ -r /etc/os-release ]; then
    . /etc/os-release
    if [ "$ID" = "rhel" ] || [ "$ID" = "centos" ] || [ "$ID" = "fedora" ]; then
        install_on_rhel
    elif [ "$ID" = "debian" ] || [ "$ID" = "ubuntu" ] || [ "$ID" = "pop" ]; then
        install_on_debian
    else
        info "Unsupported distribution: $ID"
        exit 1
    fi
else
    info "Cannot determine Linux distribution."
    exit 1
fi

# Install homebrew
info "Install Homebrew"
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Install packages with homebrew
info "Install packages with Homebrew"
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
brew bundle install --global
