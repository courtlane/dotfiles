#!/bin/bash

# Function to display INFO messages
info() {
    echo -e "\e[96m[INFO]\e[0m $1"
}

# Function to display usage information
usage() {
    echo "Usage: $0 [OPTIONS]"
    echo "Options:"
    echo "  --extensions <path>   Specify the path to file with a list of Codium extensions."
    echo "  --help              Display this help message."
}

# Display usage information when --help or -h option is provided
if [[ "$1" == "--help" || "$1" == "-h" ]]; then
    usage
    exit 0
fi

# Function to install VSCodium on RHEL-based systems
install_on_rhel() {
    info "Installing VSCodium on RHEL-based systems"

    sudo rpmkeys --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg
    if [ ! -f /etc/yum.repos.d/vscodium.repo ]; then
		printf "[gitlab.com_paulcarroty_vscodium_repo]\nname=download.vscodium.com\nbaseurl=https://download.vscodium.com/rpms/\nenabled=1\ngpgcheck=1\nrepo_gpgcheck=1\ngpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg\nmetadata_expire=1h" | sudo tee -a /etc/yum.repos.d/vscodium.repo
		fi

		sudo dnf install codium -y
}

# Function to install VSCodium on Debian-based systems
install_on_debian() {
    info "Installing VSCodium on Debian-based systems"
		wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg

    if [ ! -f /etc/apt/sources.list.d/vscodium.list ]; then
		echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list
		fi

		sudo apt update && sudo apt install codium -y
}

install_extensions() {
    local extensions_file="$1"

    # Check if codium-extensions.txt exists
    if [ -f "$extensions_file" ]; then
        info "Installing Codium extensions from $extensions_file"
        # Read each extensions name from codium-extensions.txt and install it
        while IFS= read -r extension; do
            codium --install-extension "$extension" --force
        done < "$extensions_file"
    else
        echo "Error: $extensions_file not found." >&2
        exit 1
    fi
}

# Determine the Linux distribution
if [ -r /etc/os-release ]; then
    . /etc/os-release
    if [ "$ID" = "rhel" ] || [ "$ID" = "centos" ] || [ "$ID" = "fedora" ]; then
        install_on_rhel
    elif [ "$ID" = "debian" ] || [ "$ID" = "ubuntu" ] || [ "$ID" = "pop" ]; then
        install_on_debian
    else
        info "Unsupported distribution: $ID"
        exit 1
    fi

    # Check for options
    while [[ $# -gt 0 ]]; do
        case "$1" in
            --extensions)
                shift
                install_extensions "$1"
                ;;
            --help)
                usage
                exit 0
                ;;
            *)
                echo "Error: Invalid option: $1" >&2
                usage
                exit 1
                ;;
        esac
        shift
    done

else
    info "Cannot determine Linux distribution."
    exit 1
fi
