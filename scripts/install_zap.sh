#!/bin/bash

# Function to display INFO messages
info() {
    echo -e "\e[96m[INFO]\e[0m $1"
}

# Function to display ERROR messages and exit script
error() {
    echo -e "\e[91m[ERROR]\e[0m $1"
    exit 1
}

# Install zap (zsh plugin manager) --keep: do not overwrite existing .zshrc file
info "Install zap (zsh plugin manager"
zsh <(curl -s https://raw.githubusercontent.com/zap-zsh/zap/master/install.zsh) --branch release-v1 --keep 
