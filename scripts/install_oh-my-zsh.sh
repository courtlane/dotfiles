#!/bin/bash

# Function to display INFO messages
info() {
    echo -e "\e[96m[INFO]\e[0m $1"
}

# Function to display USAGE information
usage() {
    echo "Usage: $0 [-s SHELL]"
    echo "Options:"
    echo "  -s, --shell SHELL   Change login shell to SHELL"
    exit 1
}

# Install oh-my-zsh
info "Install oh-my-zsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended --keep-zshrc

# Parse command line options
while [[ "$#" -gt 0 ]]; do
    case "$1" in
        -s|--shell )
            SHELL_OPTION="$2"
            shift
            ;;
        * )
            usage
            ;;
    esac
    shift
done

# Check if the -s zsh option was provided
if [[ "$SHELL_OPTION" == "zsh" ]]; then
    # Change login shell to zsh
    info "Change login shell to zsh"
    chsh -s /bin/zsh
else
    info "Login shell remains unchanged. Use -s zsh to change to zsh."
fi
